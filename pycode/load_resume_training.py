from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
import h5py
import cv2
import os
import glob
import scipy
import pandas as pd

from keras.models import Sequential, model_from_json
from keras.layers.core import Flatten, Dense, Dropout, Reshape, Lambda
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import Adadelta, Adam, SGD
from keras.utils.np_utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping, History, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator

from collections import Counter
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.layers import Input, Flatten, Dense
from keras.models import Model, Sequential

json_file = open('m_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
m = model_from_json(loaded_model_json)
# load weights into new model
m.load_weights("../tmp/weights.h5")
print("Loaded model from disk")


nb_train_samples = len(glob.glob('../data/train/*/*.jpg'))
nb_validation_samples = len(glob.glob('../data/valid/*/*.jpg'))
nb_epoch = 45
batch_size = 16
nb_test_samples = 1000
size=(224,224)
#train

classes = ['ALB', 'BET', 'DOL', 'LAG', 'NoF', 'OTHER', 'SHARK', 'YFT']
train_datagen = ImageDataGenerator(
        featurewise_center=True,
        shear_range=0.2,
        zoom_range=0.1,
        rotation_range=10.,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

train_datagen.mean = np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((1, 1, 3))
train_generator = train_datagen.flow_from_directory(
        '../data/train/',
        target_size=size,
        batch_size=batch_size,
        shuffle = True,
        classes=classes,
        class_mode='categorical')
#valid
valid_datagen = ImageDataGenerator(
        featurewise_center=True)
valid_datagen.mean = np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((1, 1, 3))
validation_generator = valid_datagen.flow_from_directory(
        '../data/valid/',
        target_size=size,
        batch_size=batch_size,
        shuffle = True,
        classes=classes,
        class_mode='categorical')


optimizer = SGD(lr = 0.005, momentum = 0.9, decay = 1e-6, nesterov = True)
callbacks = [EarlyStopping(monitor='val_loss', patience=1, verbose=0),
           ModelCheckpoint(filepath='../tmp/weights.h5', monitor='val_acc', verbose=1,save_best_only=True)]
m.compile(loss='categorical_crossentropy', optimizer='Adadelta',metrics=["accuracy"])

hist = m.fit_generator(
        train_generator,
        steps_per_epoch=nb_train_samples/batch_size,
        epochs=nb_epoch,
        validation_data=validation_generator,
        validation_steps=nb_validation_samples,
        callbacks=callbacks)