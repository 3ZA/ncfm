from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
import h5py
import cv2
import os
import glob
import scipy
import pandas as pd

from keras.models import Sequential, model_from_json
from keras.layers.core import Flatten, Dense, Dropout, Reshape, Lambda
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import Adadelta, Adam, SGD
from keras.utils.np_utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping, History, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator

from collections import Counter
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.layers import Input, Flatten, Dense
from keras.models import Model, Sequential

json_file = open('m_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
m = model_from_json(loaded_model_json)
# load weights into new model
m.load_weights("../tmp/weights.h5")
print("Loaded model from disk")

nb_epoch = 5
batch_size = 16
nb_test_samples = 1000
size=(224,224)

test_aug = 5
test_datagen = ImageDataGenerator(
        shear_range=0.2,
        zoom_range=0.1, 
        rotation_range=10., 
        width_shift_range=0.2, 
        height_shift_range=0.2, 
        horizontal_flip=True,
        featurewise_center=True)
test_datagen.mean=np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((1, 1, 3))
for aug in range(test_aug):
    print('Predictions for Augmented -', aug)
    random_seed = np.random.randint(0, 100000)
    test_generator = test_datagen.flow_from_directory(
            '../data/test/',
            target_size=size,
            batch_size=batch_size,
            shuffle = False,
            seed = random_seed,
            classes = [''],
            class_mode = None)
    test_image_list = test_generator.filenames
    if aug == 0:
        predictions = m.predict_generator(test_generator, nb_test_samples)
    else:
        predictions += m.predict_generator(test_generator, nb_test_samples)
predictions /= test_aug

print('Begin to write submission file ..')
f_submit = open(os.path.join('/home/ubuntu/','submit'+'.csv'), 'w')
f_submit.write('image,ALB,BET,DOL,LAG,NoF,OTHER,SHARK,YFT\n')
for i, image_name in enumerate(test_generator.filenames):
    pred = ['%.6f' % p for p in predictions[i, :]]
    if i%100 == 0:
        print(i, '/', 1000)
    f_submit.write('%s,%s\n' % (os.path.basename(image_name), ','.join(pred)))

f_submit.close()