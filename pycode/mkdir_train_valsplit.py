% cd /home/trooper/Documents/Nature
% rm -r train
% rm -r valid
% mkdir train valid
% cp -r train_orig/* train
% cd train

# pool_counter = 1

for fish_class in glob.glob('*'): os.mkdir('../valid/' + fish_class)

rnd_ord = np.random.permutation(glob.glob('*/*.jpg'))
for i in range(500): os.rename(rnd_ord[i], '../valid/' + rnd_ord[i])