import glob
import os

'''BBox-Label-Tool only reads images with file extensions .JPEG'''
image_dir = '/home/jj/Documents/cnnfish code/Yolo/BBox-Label-Tool/Images'

images = glob.glob('*/*.jpg')
images = sorted(images)

for i in images:
	if i.endswith('.jpg'):	
 		os.rename(i, i[:-3]+'JPEG')
