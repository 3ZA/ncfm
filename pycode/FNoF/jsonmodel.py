from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
import h5py
import cv2
import os
import glob
import scipy
import pandas as pd

from keras.models import Sequential, model_from_json
from keras.layers.core import Flatten, Dense, Dropout, Reshape, Lambda
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import Adadelta, Adam, SGD
from keras.utils.np_utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping, History, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from collections import Counter
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.layers import Input, Flatten, Dense
from keras.models import Model, Sequential

model = VGG16(weights='imagenet')
for layer in model.layers:
    layer.trainable=False
	
input= Input(shape=(224,224,3), name= 'image_input')
fc1= model.layers[-3]
fc2= model.layers[-2]
predictions = model.layers[-1]

l1=BatchNormalization()
l2= Dropout(0.2)
l3=Dense(512, activation='relu')
l4=BatchNormalization()
l5=Dropout(0.3)
l6=Dense(1, activation='sigmoid')

x=l1(fc1.output)
x=l2(x)
x=l3(x)
x=l4(x)
x=l5(x)
predictors = l6(x)
m = Model(inputs=model.input, outputs=predictors)
model_json = m.to_json()
with open("FNoFmodel.json", "w") as json_file:
    json_file.write(model_json)