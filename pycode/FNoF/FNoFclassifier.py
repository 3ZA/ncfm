from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
import h5py
import cv2
import os
import glob
import scipy
import pandas as pd

from keras.models import Sequential, model_from_json
from keras.layers.core import Flatten, Dense, Dropout, Reshape, Lambda
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import Adadelta, Adam, SGD
from keras.utils.np_utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping, History, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from collections import Counter
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.layers import Input, Flatten, Dense
from keras.models import Model, Sequential

model = VGG16(weights='imagenet')
for layer in model.layers:
    layer.trainable=False
	
input= Input(shape=(224,224,3), name= 'image_input')
fc1= model.layers[-3]
fc2= model.layers[-2]
predictions = model.layers[-1]

l1=BatchNormalization()
l2= Dropout(0.2)
l3=Dense(512, activation='relu')
l4=BatchNormalization()
l5=Dropout(0.3)
l6=Dense(1, activation='sigmoid')

x=l1(fc1.output)
x=l2(x)
x=l3(x)
x=l4(x)
x=l5(x)
predictors = l6(x)
m = Model(inputs=model.input, outputs=predictors)

nb_train_samples = len(glob.glob('../data/train/*/*.JPEG'))
nb_validation_samples = len(glob.glob('../data/valid/*/*.JPEG'))
nb_epoch = 5
batch_size = 8
nb_test_samples = 92
size=(224,224)

classes = ['F','NoF']

#train
train_datagen = ImageDataGenerator(
        featurewise_center=True,
        shear_range=0.2,
        zoom_range=0.1,
        rotation_range=10.,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)
train_datagen.mean = np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((1, 1, 3))
train_generator = train_datagen.flow_from_directory(
        '../data/orig_train/',
        target_size=size,
        batch_size=batch_size,
        shuffle = True,
        classes=classes,
        class_mode='binary')
#valid
valid_datagen = ImageDataGenerator(
        featurewise_center=True)
valid_datagen.mean = np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((1, 1, 3))
validation_generator = valid_datagen.flow_from_directory(
        '../data/orig_train/',
        target_size=size,
        batch_size=batch_size,
        shuffle = True,
        classes=classes,
        class_mode='binary')


optimizer = SGD(lr = 0.005, momentum = 0.9, decay = 1e-6, nesterov = True)
callbacks = [EarlyStopping(monitor='val_loss', patience=1, verbose=0),
           ModelCheckpoint(filepath='../tmp/weights_orig.h5', monitor='val_acc', verbose=1,save_best_only=True)]
m.compile(loss='binary_crossentropy', optimizer='Adadelta',metrics=["accuracy"])

hist = m.fit_generator(
        train_generator,
        steps_per_epoch=nb_train_samples//batch_size,
        epochs=nb_epoch,
        validation_data=validation_generator,
        validation_steps=nb_validation_samples//batch_size,
        callbacks=callbacks)
#m.save_weights('jj2.h5')
print('train succesful')