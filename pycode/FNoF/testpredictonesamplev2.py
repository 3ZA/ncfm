from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
import h5py
import cv2
import os
import glob
import scipy
import pandas as pd

from keras.models import Sequential, model_from_json
from keras.layers.core import Flatten, Dense, Dropout, Reshape, Lambda
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import Adadelta, Adam, SGD
from keras.utils.np_utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.layers.normalization import BatchNormalization
from keras.callbacks import EarlyStopping, History, ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator

from collections import Counter
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.layers import Input, Flatten, Dense
from keras.models import Model, Sequential

json_file = open('FNoFmodel.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
m = model_from_json(loaded_model_json)
# load weights into new model
m.load_weights("../tmp/weights2.h5")
print("Loaded model from disk")
images=glob.glob('../data/train/F/*.JPEG')
id=[]
pictures=[]
for img in images:
	id.append(img)
	pic = cv2.imread('img')
	pic=pic.reshape((224,224,3))
	pictures.append(pic)
print(m.predict(img))
pictures= np.array(pictures)
