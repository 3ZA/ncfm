from skimage.data import imread
from skimage.io import imshow,imsave
from skimage import img_as_float
import pandas as pd
import numpy as np
import cv2
from skimage.util import crop
from skimage.transform import rotate
from skimage.transform import resize
import matplotlib.pyplot as plt
%matplotlib inline
import math
import warnings
warnings.filterwarnings("ignore")

def deg_angle_between(x1,y1,x2,y2):
    from math import atan2, degrees, pi
    dx = x2 - x1
    dy = y2 - y1
    rads = atan2(-dy,dx)
    rads %= 2*pi
    degs = degrees(rads)
    return(degs)

def get_rotated_cropped_fish(img,x1,y1,x2,y2):
    (h,w) = img.shape[:2]
    #calculate center and angle
    center = ( (x1+x2) / 2,(y1+y2) / 2)
    angle = np.floor(-deg_angle_between(x1,y1,x2,y2))
    #print('angle=' +str(angle) + ' ')
    #print('center=' +str(center))
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h))
    
    fish_length = np.sqrt((x1-x2)**2+(y1-y2)**2)
    cropped = rotated[(max(int((center[1]-fish_length/1.8)),0)):(max(int(center[1]+(fish_length/1.8)),0)) ,
                      (max(int((center[0]- fish_length/1.8)),0)):(max(int(center[0]+(fish_length/1.8)),0))]
    #imshow(img)
    #imshow(rotated)
    #imshow(cropped)
    resized = resize(cropped,(224,224))
    return(resized)

label_files = ['/home/ubuntu/data/labels/bet_labels.json',
             '/home/ubuntu/data/labels/alb_labels.json',
             '/home/ubuntu/data/labels/yft_labels.json',
             '/home/ubuntu/data/labels/dol_labels.json',
             '/home/ubuntu/data/labels/shark_labels.json',
             '/home/ubuntu/data/labels/lag_labels.json',
             '/home/ubuntu/data/labels/other_labels.json']

data_dirs = ['/home/ubuntu/data/train/BET/',
             '/home/ubuntu/data/train/ALB/',
             '/home/ubuntu/data/train/YFT/',
             '/home/ubuntu/data/train/DOL/',
             '/home/ubuntu/data/train/SHARK/',
             '/home/ubuntu/data/train/LAG/',
             '/home/ubuntu/data/train/OTHER/']

cropped_data_dirs = ['/home/ubuntu/data/cropped_train/BET/',
             '/home/ubuntu/data/cropped_train/ALB/',
             '/home/ubuntu/data/cropped_train/YFT/',
             '/home/ubuntu/data/cropped_train/DOL/',
             '/home/ubuntu/data/cropped_train/SHARK/',
             '/home/ubuntu/data/cropped_train/LAG/',
             '/home/ubuntu/data/cropped_train/OTHER/']

data_labels = ['BET','ALB','YFT','DOL','SHARK','LAG','OTHER']



images = list()
labels_list = list()
for c in range(7):
    labels = pd.read_json(label_files[c])
    for i in range(len(labels)):
        try:
            img_filename = labels.iloc[i,2]
            l1 = pd.DataFrame((labels[labels.filename==img_filename].annotations).iloc[0])
            image = imread(data_dirs[c]+img_filename)
            temp = get_rotated_cropped_fish(image,np.floor(l1.iloc[0,1]),np.floor(l1.iloc[0,2]),np.floor(l1.iloc[1,1]),np.floor(l1.iloc[1,2]))
            images.append(temp)
            imsave('../data/cropped_train/'+data_labels[c]+'/' img_filename ,temp)
            #print('success')
            labels_list.append(c)
        except:
            pass
    print('{} out of 7 complete'.format(c+1))





for i in range(50):
	fig,ax = plt.subplots(nrows=1,ncols=8,sharex="col",sharey="row",figsize=(24,3))
	fig.suptitle(str(labels_list[(i):(i+8)]),fontsize=16)
	for j in range(8):
		ax[j].imshow(images[j+i*5])
